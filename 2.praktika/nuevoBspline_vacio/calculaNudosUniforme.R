#######################################################################
###
### CALCULA LOS NUDOS UNIFORMES DE UNA BSPLINE
### @params: nDatos (numero de datos)
### @params: p (grado de la bspline)
###
### Devolvemos un vector de nudos
###
#######################################################################
calculaNudosUniforme <- function(nDatos, p) {

    ### CODIGO A REALIZAR:
    ### Calcular los nudos uniformes de una bspline

    lVector <- c(0,0,0,0)
    rVector <- c(1,1,1,1)
    
    #calcular uniformes
    cVector <- 0
    incr <- 1 / (nDatos-p+2)
    for(i in 1:nDatos-2)
      cVector[i]<-i*incr

    #juntar los vectores    
    nudos <- c(lVector, cVector, rVector)

  	return(nudos)
}